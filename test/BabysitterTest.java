import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class BabysitterTest {

    private Babysitter babysitter = new Babysitter();

    @Test
    public void checkInvalidStartTime() {
        babysitter.println("Checking invalid start time.");

        int[] testCases = {-1, 0, 1, 400, 1699, 2301, 2359, 2400, 2401};

        for (int startTime : testCases) {
            babysitter.startTime = startTime;
            assertFalse(babysitter.checkStartTime());
        }
    }

    @Test
    public void checkValidStartTime() {
        babysitter.println("Checking valid start time.");

        int[] testCases = {1700, 1802, 2300, 2259};

        for (int startTime : testCases) {
            babysitter.startTime = startTime;
            assertTrue(babysitter.checkStartTime());
        }
    }

    @Test
    public void checkValidEndTimePostMidnight() {
        babysitter.println("Checking valid endtime.");

        int startTime = 1700;
        int[] testCases = {0, 100, 200, 300, 301, 359, 400};

        for (int endTime : testCases) {
            babysitter.startTime = startTime;
            babysitter.endTime = endTime;
            assertTrue(babysitter.checkEndTime());
        }
    }

    @Test
    public void checkValidEndTimePreMidnight() {
        babysitter.println("Checking valid endtime post midnight.");

        int startTime = 1700;
        int[] testCases = {1701, 1800, 1900, 2359, 0};

        for (int endTime : testCases) {
            babysitter.startTime = startTime;
            babysitter.endTime = endTime;
            assertTrue(babysitter.checkEndTime());
        }
    }

    @Test
    public void checkInvalidEndTimePostMidnight() {
        babysitter.println("Checking invalid endtime post midnight.");

        int startTime = 1700;
        int[] testCases = {401, 500, 1200, 2400, 2599};

        for (int endTime : testCases) {
            babysitter.startTime = startTime;
            babysitter.endTime = endTime;
            assertFalse(babysitter.checkEndTime());
        }
    }

    @Test
    public void checkInvalidEndTimePreMidnight() {
        babysitter.println("Checking invalid endtime pre-midnight.");

        int startTime = 2300;
        int[] testCases = {1600, 1700, 2259, 2300};

        for (int endTime : testCases) {
            babysitter.startTime = startTime;
            babysitter.endTime = endTime;
            assertFalse(babysitter.checkEndTime());
        }
    }

    @Test
    public void checkValidBedTimePostMidnightEndTime() {
        babysitter.println("Checking valid bedtime if end time is post midnight.");

        int startTime = 1700;
        int endTime = 400;
        int[] testCases = {1700, 1800, 1850, 2359, 0};

        for (int bedTime : testCases) {
            babysitter.startTime = startTime;
            babysitter.endTime = endTime;
            babysitter.bedTime = bedTime;
            assertTrue(babysitter.checkBedTime());
        }
    }

    @Test
    public void checkValidBedTimePreMidnightEndTime() {
        babysitter.println("Checking valid bedtime if end time is pre-midnight.");

        int startTime = 1700;
        int endTime = 2300;
        int[] testCases = {1700, 1800, 1850};

        for (int bedTime : testCases) {
            babysitter.startTime = startTime;
            babysitter.endTime = endTime;
            babysitter.bedTime = bedTime;
            assertTrue(babysitter.checkBedTime());
        }
    }

    @Test
    public void roundTime() {
        babysitter.println("Checking roundtime");

        int[] testCases = {1700, 1802, 1959, 2300, 2334, 0, 1, 102, 309};
        int[] expectedValues = {1700, 1900, 2000, 2300, 0, 0, 100, 200, 400};

        for (int i = 0; i < testCases.length; i++) {
            assertEquals(babysitter.roundTime(testCases[i]), expectedValues[i]);
        }
    }

    @Test
    public void testCalculateRate() {
        babysitter.println("Checking calculate rate.");

        List<int[]> testCases = Arrays.asList(
                new int[]{1700, 0, 400, 148},
                new int[]{1800, 1800, 400, 208},
                new int[]{1700, 2200, 400, 172},
                new int[]{1800, 0, 0, 72},
                new int[]{1700, 1700, 0, 168},
                new int[]{2300, 0, 0, 12},
                new int[]{2300, 0, 100, 28},
                new int[]{1900, 2200, 300, 132}
        );

        for (int[] times : testCases) {
            babysitter.startTime = times[0];
            babysitter.bedTime = times[1];
            babysitter.endTime = times[2];
            int expectedValue = times[3];
            int calculatedRate = babysitter.calculateRate();
            assertEquals(calculatedRate, expectedValue);
        }
    }

}