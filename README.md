# Babysitter Kata

## Background
This kata simulates a babysitter working and getting paid for one night.  The rules are pretty straight forward.

The babysitter:

- starts no earlier than 5:00PM
- leaves no later than 4:00AM
- gets paid $12/hour from start-time to bedtime
- gets paid $8/hour from bedtime to midnight
- gets paid $16/hour from midnight to end of job
- gets paid for full hours (no fractional hours)


## Feature

* As a babysitter *

In order to get paid for 1 night of work

I want to calculate my nightly charge



## To Run
* Open project in IntelliJ
* Build and run `Main`.
* Consider runing ./gradlew build` however, the `run` task is not properly setup.

## To Test
* Same as run however, change configuration to run jUnit.
* `./graldew test` may work.

### TODO
* code cleanup.
* ensure consistency between 0000 and 2400 hours.
* expose times via get/set over protected access level.
* most testing around rounded pay.
* removal of magic numbers.
* documentation of if-else logic at a high level.

