import java.util.Scanner;

public class Babysitter {

    private Scanner in = new Scanner(System.in);
    int startTime = -1;
    int endTime = -1;
    int bedTime = -1;

    private final int PRE_BEDTIME_RATE = 12;
    private final int POST_BEDTIME_RATE = 8;
    private final int POST_MIDNIGHT_RATE = 16;
    private final int MIDNIGHT = 2400;

    public void main() {

        printIntro();

        getStartTime();
        getEndTime();
        getBedTime();

        System.out.println("The rate is: $" + calculateRate() + ".");

        in.close();
    }

    int calculateRate() {

        startTime = roundTime(startTime);
        endTime = roundTime(endTime);
        bedTime = roundTime(bedTime);

        if (bedTime == 0) {
            bedTime = MIDNIGHT;
        }

        int preBedTime = PRE_BEDTIME_RATE * (bedTime - startTime);
        int postBedTime = POST_BEDTIME_RATE * (MIDNIGHT - bedTime);

        int postMidnightTime = 0;
        postMidnightTime = POST_MIDNIGHT_RATE * (MIDNIGHT - bedTime);
        if (endTime + MIDNIGHT <= MIDNIGHT + 400) {
            postMidnightTime += POST_MIDNIGHT_RATE * (endTime);
        }

        return (preBedTime + postBedTime + postMidnightTime) / 100;
    }

    private void getStartTime() {
        println("Enter start time no later than 5PM (1700) and no earlier than 11 PM (2300): ");
        do {
            startTime = in.nextInt();
            in.nextLine();
        } while (!checkStartTime());

        println("");
    }

    private void getEndTime() {
        print("Enter end time no earlier than 4 AM (0400). You cannot end before you start: ");
        do {
            endTime = in.nextInt();
            in.nextLine();
        } while (!checkEndTime());

        println("");
    }

    private void getBedTime() {
        print("Enter bed time no later than midnight (0000). Note, you cannot have a bedtime past the end time nor can bedtime be before the start time: ");
        do {
            bedTime = in.nextInt();
            in.nextLine();
        } while (!checkBedTime());

        println("");
    }

    boolean checkStartTime() {
        boolean b = startTime >= 1700 && startTime <= 2300;
        if (!b) print("\nNot a valid start time. Enter again: ");
        return b;
    }

    boolean checkEndTime() {
        boolean b = (endTime <= 400 && endTime >= 0) ||
                (endTime <= 2359 && endTime > startTime);
        if (!b) print("\nNot a valid end time. Enter again: ");
        return b;
    }

    boolean checkBedTime() {
        boolean b;
        if (endTime + MIDNIGHT >= MIDNIGHT + 400 || endTime == 0) { /* Check if endtime is after midnight. */
            b = (bedTime >= startTime && bedTime <= 2359) || bedTime == 0;
        } else {
            b = (bedTime >= startTime && bedTime <= endTime) || bedTime == 0;
        }

        if (!b) print("\nNot a valid bedtime. Enter again: ");
        return b;
    }

    int roundTime(int time) {
        time = ((time + 99) / 100) * 100;
        if (time == 2400) time = 0;
        return time;
    }

    void printIntro() {
        println("Babysitter Kata");
        println("@author: Nikit Malkan\n");

        println("Enter the times as using the 24 time standard format.");
        println("For example, 5 AM is 0500 whereas 5 PM is 1700.");
        println("Valid entries are 0000 to 2359.\n");

        println("For the sake of simplicity, the following assumptions have been made:");
        println("* Babysitter can start at the latest 11 PM (2300).");
        println("* Any fractional hours (ex: 2354 or 1732) will be rounded UP to the nearest hour.");
        println("* This means that both 2254 and 2201 would become 2300. 2301 would become 0000 for midnight.");
        println("* A number will always be used for input.");
        println("\n   ~ ~ ~   \n");
    }

    void println(Object object) {
        System.out.println(object);
    }

    void print(Object object) {
        System.out.print(object);
    }
}
